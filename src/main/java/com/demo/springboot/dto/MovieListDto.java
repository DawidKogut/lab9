package com.demo.springboot.dto;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MovieListDto {
    private List<MovieDto> movies;

    public MovieListDto(List<MovieDto> movies) {
        this.movies = movies;
    }

    public List<MovieDto> getMovies() {
        return movies;
    }

    @Override
    public String toString() {
        return IntStream.range(0, movies.size())
                .mapToObj(index -> index)
                .collect(Collectors.toList()).toString();
    }

    public MovieDto getItem(int movieId){
        return movies.get(movieId);
    }

    public MovieDto deleteItem(int movieId){
        return movies.remove(movieId);
    }
}
