package com.demo.springboot.rest;

import com.demo.springboot.Movies;
import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.dto.MovieListDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
public class MovieApiController {
    private static final Logger LOG = LoggerFactory.getLogger(MovieApiController.class);

    Movies movies = new Movies();

    public MovieApiController() {}

    @PostMapping("/api/users")
    public ResponseEntity<Void> createMovie(@RequestBody MovieDto createMovieDto) throws URISyntaxException {
        LOG.info("--- title: {}", createMovieDto.getTitle());
        movies.addMovie(createMovieDto);

        return ResponseEntity.created(new URI("/api/users/" + createMovieDto)).build();
    }

    @GetMapping("/api/users")
    public ResponseEntity<MovieListDto> getMovies() {
        LOG.info("--- get all movies: {}", movies.moviesList);
        return ResponseEntity.ok().body(movies.moviesList);    // = new ResponseEntity<>(movies, HttpStatus.OK);
    }

    @PutMapping("/api/users/{id}")
    public ResponseEntity<Void> updateMovie(@PathVariable String id, @RequestBody MovieDto createMovieDto) throws URISyntaxException {
        LOG.info("--- id: {}", id);
        movies.updateMovie(Integer.parseInt(id), createMovieDto);

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/api/users/{id}")
    public ResponseEntity<Void> deleteMovie(@PathVariable String id) {
        LOG.info("--- delete id: {}", id);
        movies.deleteMovie(Integer.parseInt(id));

        return ResponseEntity.ok().build();
    }

}
