package com.demo.springboot;

import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.dto.MovieListDto;

import java.util.ArrayList;
import java.util.List;

public class Movies {
    public MovieListDto moviesList;

    private List<MovieDto> moviesListDto = new ArrayList<>();

    public Movies(){

    }

    public void addMovie(MovieDto item){
        moviesListDto.add(item);
        moviesList = new MovieListDto(moviesListDto);
    }

    public MovieDto updateMovie(int movieId, MovieDto item){
        try {
            moviesList.getItem(movieId).setTitle(item.getTitle());
            moviesList.getItem(movieId).setYear(item.getYear());
            moviesList.getItem(movieId).setImage(item.getImage());

            return moviesList.getItem(movieId);
        }
        catch(Exception e) {
            System.out.println("Element not found");
            return null;
        }
    }

    public MovieDto deleteMovie(Integer movieId){
        try {
            return moviesList.deleteItem(movieId);
        }
        catch(Exception e) {
            System.out.println("Element not found");
            return null;
        }
    }
}
